import unittest
from json_reader import read_json, parse_data, write_json


class TestReader(unittest.TestCase):
	def test_json_reader(self):
		output_dict = read_json("http://therecord.co/feed.json")
		self.assertEqual(output_dict["home_page_url"], "http://therecord.co/")
		output_dict = read_json("some dummy url")
		self.assertEqual(output_dict, None)


	def test_parse_data(self):
		test_input = {"author": {"name": "Hue BUI"},
					  "items": [{"title": "Article 1 - Ethan B"}, {"title": "Article 2 - Bao N"}]}
		test_output = {"people": ["Hue BUI", "Ethan B", "Bao N"], "is_success": True}
		self.assertEqual(parse_data(test_input), test_output)
		test_output = {"people": [], "is_success": False}
		self.assertEqual(parse_data(None), test_output)


	def test_write_json(self):
		input_dict = {"key1": "value1", "key2": "value2"}
		output_json = '{\n  "key1": "value1",\n  "key2": "value2"\n}'
		self.assertEqual(write_json(input_dict), output_json)


if __name__ == "__main__":
	unittest.main()