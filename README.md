# axon-homework

For the second task which is to package the service as a container and deploy it
to a k8s cluster, please run the command below to run the docker image:

```sh
docker run hbui-microservice:1.0
```

For the k8s cluster, I am not clear in which cluser I should deploy the service,
I just provide the yaml config which I already tested in my local cluster.